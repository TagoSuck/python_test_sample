import unittest
from addition import add_num
from mycalc import MyCalc

class TestCalc1(unittest.TestCase):
    def test_add_num(self):
        """
        Test for the add_num function in the addition.py
        """
        value1 = 8
        value2 = 5
        expected = 13
        self.assertEqual(add_num(value1, value2), expected)

    def test_addition(self):
        """
        Test for the add_num method of the MyClac class in the mycalc.py
        """
        value1 = 5
        value2 = 6
        expected = 11
        mycalc = MyCalc()
        self.assertEqual(mycalc.add_num(value1, value2), expected)

    def test_subtraction(self):
        """
        Test for the add_num method of the MyCalc class in the mycalc.py
        """
        value1 = 11
        value2 = 5
        expected = 6
        mycalc = MyCalc()
        self.assertEqual(mycalc.sub_num(value1, value2), expected)

class TestCalc2(unittest.TestCase):
    def setUp(self):
        print "setUp"
        self.mycalc = MyCalc()

    def test_add_num(self):
        """
        Test for the add_num function in the addition.py
        """
        value1 = 8
        value2 = 5
        expected = 13
        self.assertEqual(add_num(value1, value2), expected)

    def test_addition(self):
        """
        Test for the add_num method of the MyClac class in the mycalc.py
        """
        value1 = 5
        value2 = 6
        expected = 11
        self.assertEqual(self.mycalc.add_num(value1, value2), expected)

    def test_subtraction(self):
        """
        Test for the add_num method of the MyCalc class in the mycalc.py
        """
        value1 = 11
        value2 = 5
        expected = 6
        self.assertEqual(self.mycalc.sub_num(value1, value2), expected)

    def tearDown(self):
        print "tearDown"
        del self.mycalc

if __name__ == "__main__":
    unittest.main()
